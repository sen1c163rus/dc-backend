"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const fastify_1 = __importDefault(require("fastify"));
const shared_1 = __importDefault(require("@serpand/shared"));
const server = (0, fastify_1.default)();
server.get('/', async (request, reply) => {
    return shared_1.default.plus(4, 4);
});
server.listen({ port: 8080 }, (err, address) => {
    if (err) {
        console.error(err);
        process.exit(1);
    }
    console.log(`Server listening at ${address}`);
    console.log({
        isObject: true,
        message: "Hello world!",
        objectInObject: {
            message: " I'm object",
        },
    });
});
