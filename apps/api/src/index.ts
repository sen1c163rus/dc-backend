import fastify from 'fastify';
import { MessageBroker } from '@serpand/shared';

const server = fastify();
server.get('/', async function (request, response) {
  const messageBroker = MessageBroker.getInstance();
  try {
    const res = await messageBroker.request('request.test', {
      text: 'qwe1',
    });

    console.log(res);
  } catch (e) {
    console.log(e);
  }
  return 4 + 4;
});
server.get('/test', async function (request, response) {
  const messageBroker = MessageBroker.getInstance();
  try {
    const res = await messageBroker.send('send.test', {
      text: 'qwe',
    });

    console.log(res);
  } catch (e) {
    console.log(e);
  }
  return 4 + 4;
});

// eslint-disable-next-line @typescript-eslint/no-misused-promises
server.listen({ port: 8080 }, async function (err, address) {
  if (err != null) {
    console.error(err);
    process.exit(1);
  }

  try {
    void new MessageBroker({ servers: 'localhost:4222' });
    const messageBroker = MessageBroker.getInstance();
    await messageBroker.connect();
  } catch (error) {
    console.error(error.message);
  }

  console.log(`Start "API" service listening at ${address}`);
});
