const baseConfig = require("@serpand/config-eslint");

module.exports = {
  ...baseConfig,
  parserOptions: {
    ...baseConfig.parserOptions,
    project: ["./tsconfig.json"],
  },
};
