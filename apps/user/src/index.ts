import fastify from 'fastify';
import { MessageBroker } from '@serpand/shared';

const server = fastify();

// eslint-disable-next-line @typescript-eslint/no-misused-promises
server.listen({ port: 8090 }, async function (err, address) {
  if (err != null) {
    console.error(err);
    process.exit(1);
  }

  const messageBroker = new MessageBroker({ servers: 'localhost:4222' });
  await messageBroker.connect();
  messageBroker.subscribeRequest('request.test', (data) => {
    return {
      data: `Hello. It is response! :) Your data: ${String(data.text)}`,
    };
  });

  messageBroker.subscribeSend('send.test', () => {
    return true;
  });
  console.log(`Start "User" service listening at ${address}`);
});
