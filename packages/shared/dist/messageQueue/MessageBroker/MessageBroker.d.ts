import { CallbackRequest, ConstructorParams, IMessageBroker, QueueRequest, QueuesRequest, Servers } from '.';
import { QueueSend, QueuesSend } from './queues.send.types';
export declare class MessageBroker implements IMessageBroker {
    #private;
    constructor({ servers }: ConstructorParams);
    connect(): Promise<true>;
    send<Q extends QueueSend>(queue: Q, data: QueuesSend[Q]): Promise<true>;
    request<Q extends QueueRequest>(queue: Q, data: QueuesRequest[Q]['message']): Promise<QueuesRequest[Q]['response']>;
    subscribeSend<Q extends QueueSend>(queue: Q, callback: (message: QueuesSend[Q]) => void): boolean;
    subscribeRequest<Q extends QueueRequest>(queue: Q, callback: CallbackRequest): true;
    get servers(): Servers;
    static getInstance(): MessageBroker;
}
