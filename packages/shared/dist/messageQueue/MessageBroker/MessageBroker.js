"use strict";
var __classPrivateFieldSet = (this && this.__classPrivateFieldSet) || function (receiver, state, value, kind, f) {
    if (kind === "m") throw new TypeError("Private method is not writable");
    if (kind === "a" && !f) throw new TypeError("Private accessor was defined without a setter");
    if (typeof state === "function" ? receiver !== state || !f : !state.has(receiver)) throw new TypeError("Cannot write private member to an object whose class did not declare it");
    return (kind === "a" ? f.call(receiver, value) : f ? f.value = value : state.set(receiver, value)), value;
};
var __classPrivateFieldGet = (this && this.__classPrivateFieldGet) || function (receiver, state, kind, f) {
    if (kind === "a" && !f) throw new TypeError("Private accessor was defined without a getter");
    if (typeof state === "function" ? receiver !== state || !f : !state.has(receiver)) throw new TypeError("Cannot read private member from an object whose class did not declare it");
    return kind === "m" ? f : kind === "a" ? f.call(receiver) : f ? f.value : state.get(receiver);
};
var _MessageBroker_instances, _MessageBroker_servers, _MessageBroker_subscriptions, _MessageBroker_stringCodec, _MessageBroker_connection, _MessageBroker_dataEncode, _MessageBroker_dataDecode;
Object.defineProperty(exports, "__esModule", { value: true });
exports.MessageBroker = void 0;
const constants_1 = require("./constants");
const nats_1 = require("nats");
let instance;
class MessageBroker {
    constructor({ servers }) {
        _MessageBroker_instances.add(this);
        _MessageBroker_servers.set(this, void 0);
        _MessageBroker_subscriptions.set(this, void 0);
        _MessageBroker_stringCodec.set(this, (0, nats_1.StringCodec)());
        _MessageBroker_connection.set(this, null);
        __classPrivateFieldSet(this, _MessageBroker_servers, servers, "f");
        __classPrivateFieldSet(this, _MessageBroker_subscriptions, new Map(), "f");
        instance = this;
    }
    async connect() {
        try {
            __classPrivateFieldSet(this, _MessageBroker_connection, await (0, nats_1.connect)({
                servers: __classPrivateFieldGet(this, _MessageBroker_servers, "f"),
                reconnectTimeWait: 15 * 1000,
            }), "f");
            console.info('NATS Connected');
            return true;
        }
        catch (error) {
            let errorCode;
            switch (error.code) {
                case nats_1.ErrorCode.ConnectionRefused:
                    errorCode = constants_1.ErrorCode.ConnectionRefused;
                    break;
                default:
                    errorCode = constants_1.ErrorCode.UnknownError;
            }
            throw new Error(errorCode);
        }
    }
    async send(queue, data) {
        if (!__classPrivateFieldGet(this, _MessageBroker_connection, "f"))
            throw new Error(constants_1.ErrorCode.NotConnected);
        const dataEncode = __classPrivateFieldGet(this, _MessageBroker_instances, "m", _MessageBroker_dataEncode).call(this, data);
        try {
            __classPrivateFieldGet(this, _MessageBroker_connection, "f").publish(queue, dataEncode);
            return true;
        }
        catch (error) {
            console.log(error);
            throw new Error(constants_1.ErrorCode.MessageNotSent);
        }
    }
    async request(queue, data) {
        if (!__classPrivateFieldGet(this, _MessageBroker_connection, "f"))
            throw new Error(constants_1.ErrorCode.NotConnected);
        const dataEncode = __classPrivateFieldGet(this, _MessageBroker_instances, "m", _MessageBroker_dataEncode).call(this, data);
        try {
            const response = await __classPrivateFieldGet(this, _MessageBroker_connection, "f").request(queue, dataEncode);
            return __classPrivateFieldGet(this, _MessageBroker_instances, "m", _MessageBroker_dataDecode).call(this, response.data);
        }
        catch (error) {
            if (error.code && error.code === '503') {
                throw new Error(constants_1.ErrorCode.NoSubscriber);
            }
            throw new Error(constants_1.ErrorCode.MessageNotSent);
        }
    }
    subscribeSend(queue, callback) {
        if (!__classPrivateFieldGet(this, _MessageBroker_connection, "f"))
            throw new Error(constants_1.ErrorCode.NotConnected);
        const subscription = __classPrivateFieldGet(this, _MessageBroker_connection, "f").subscribe(queue);
        if (!subscription)
            return false;
        (async () => {
            for await (const message of subscription) {
                callback(__classPrivateFieldGet(this, _MessageBroker_instances, "m", _MessageBroker_dataDecode).call(this, message.data));
            }
        })();
        return true;
    }
    subscribeRequest(queue, callback) {
        if (!__classPrivateFieldGet(this, _MessageBroker_connection, "f"))
            throw new Error(constants_1.ErrorCode.NotConnected);
        const subscription = __classPrivateFieldGet(this, _MessageBroker_connection, "f").subscribe(queue);
        if (!subscription)
            throw new Error(constants_1.ErrorCode.SubscribeNotSuccess);
        (async () => {
            for await (const message of subscription) {
                const callbackResult = await callback(__classPrivateFieldGet(this, _MessageBroker_instances, "m", _MessageBroker_dataDecode).call(this, message.data));
                message.respond(__classPrivateFieldGet(this, _MessageBroker_instances, "m", _MessageBroker_dataEncode).call(this, callbackResult));
            }
        })();
        return true;
    }
    get servers() {
        return __classPrivateFieldGet(this, _MessageBroker_servers, "f");
    }
    static getInstance() {
        return instance;
    }
}
exports.MessageBroker = MessageBroker;
_MessageBroker_servers = new WeakMap(), _MessageBroker_subscriptions = new WeakMap(), _MessageBroker_stringCodec = new WeakMap(), _MessageBroker_connection = new WeakMap(), _MessageBroker_instances = new WeakSet(), _MessageBroker_dataEncode = function _MessageBroker_dataEncode(data) {
    return __classPrivateFieldGet(this, _MessageBroker_stringCodec, "f").encode(typeof data === 'string' ? data : JSON.stringify(data));
}, _MessageBroker_dataDecode = function _MessageBroker_dataDecode(data) {
    const dataDecode = __classPrivateFieldGet(this, _MessageBroker_stringCodec, "f").decode(data);
    try {
        return JSON.parse(dataDecode);
    }
    catch (e) {
        return dataDecode;
    }
};
