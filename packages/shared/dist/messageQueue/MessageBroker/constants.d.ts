export declare enum ErrorCode {
    ConnectionRefused = "CONNECTION REFUSED",
    UnknownError = "UNKNOWN ERROR",
    NotConnected = "NOT CONNECTED",
    MessageNotSent = "MESSAGE NOT SENT",
    SubscribeNotSuccess = "SUBSCRIBE NOT SUCCESS",
    NoSubscriber = "NO SUBSCRIBER"
}
