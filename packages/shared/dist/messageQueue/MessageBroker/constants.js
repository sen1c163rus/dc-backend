"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ErrorCode = void 0;
var ErrorCode;
(function (ErrorCode) {
    ErrorCode["ConnectionRefused"] = "CONNECTION REFUSED";
    ErrorCode["UnknownError"] = "UNKNOWN ERROR";
    ErrorCode["NotConnected"] = "NOT CONNECTED";
    ErrorCode["MessageNotSent"] = "MESSAGE NOT SENT";
    ErrorCode["SubscribeNotSuccess"] = "SUBSCRIBE NOT SUCCESS";
    ErrorCode["NoSubscriber"] = "NO SUBSCRIBER";
})(ErrorCode = exports.ErrorCode || (exports.ErrorCode = {}));
