export interface QueuesRequest {
    'request.test': {
        message: {
            text: string;
        };
        response: {
            data: string;
        };
    };
}
export declare type QueueRequest = keyof QueuesRequest;
