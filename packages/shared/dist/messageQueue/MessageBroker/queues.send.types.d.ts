export interface QueuesSend {
    'send.test': {
        text: string;
    };
}
export declare type QueueSend = keyof QueuesSend;
