import { QueueRequest, QueuesRequest } from '.';
import { QueueSend, QueuesSend } from './queues.send.types';
export declare type Servers = string[] | string;
export interface ConstructorParams {
    servers: Servers;
}
export declare type Queue = QueueSend | QueueRequest;
export declare type Callback = CallbackSand | CallbackRequest;
export declare type CallbackSand = <Q extends QueueSend>(message: QueuesSend[Q]) => void;
export declare type CallbackRequest = <Q extends QueueRequest>(message: QueuesRequest[Q]['message']) => QueuesRequest[Q]['response'] | Promise<QueuesRequest[Q]['response']>;
export interface IMessageBroker {
    connect: () => Promise<true>;
    servers: Servers;
    send: <Q extends QueueSend>(queue: Q, data: QueuesSend[Q]) => Promise<true>;
    request: <Q extends QueueRequest>(queue: Q, data: QueuesRequest[Q]['message']) => Promise<QueuesRequest[Q]['response']>;
    subscribeSend: <Q extends QueueSend>(queue: Q, callback: CallbackSand) => boolean;
    subscribeRequest: <Q extends QueueRequest>(queue: Q, callback: CallbackRequest) => true;
}
