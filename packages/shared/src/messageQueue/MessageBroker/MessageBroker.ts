import { ErrorCode } from './constants';
import {
  connect,
  ErrorCode as ErrorCodeNats,
  NatsConnection,
  StringCodec,
} from 'nats';
import {
  Callback,
  CallbackRequest,
  ConstructorParams,
  IMessageBroker,
  Queue,
  QueueRequest,
  QueuesRequest,
  Servers,
} from '.';
import { QueueSend, QueuesSend } from './queues.send.types';

let instance: MessageBroker;

export class MessageBroker implements IMessageBroker {
  readonly #servers: Servers;
  #subscriptions: Map<Queue, Callback>;
  #stringCodec = StringCodec();
  #connection: NatsConnection | null = null;

  #dataEncode<R>(data: R): Uint8Array {
    return this.#stringCodec.encode(
      typeof data === 'string' ? data : JSON.stringify(data)
    );
  }

  #dataDecode<R>(data: Uint8Array): R {
    const dataDecode = this.#stringCodec.decode(data);
    try {
      return JSON.parse(dataDecode) as R;
    } catch (e) {
      return dataDecode as R;
    }
  }

  constructor({ servers }: ConstructorParams) {
    this.#servers = servers;
    this.#subscriptions = new Map();
    instance = this;
  }

  public async connect(): Promise<true> {
    try {
      this.#connection = await connect({
        servers: this.#servers,
        reconnectTimeWait: 15 * 1000,
      });
      console.info('NATS Connected');
      return true;
    } catch (error) {
      let errorCode: ErrorCode;
      switch (error.code) {
        case ErrorCodeNats.ConnectionRefused:
          errorCode = ErrorCode.ConnectionRefused;
          break;
        default:
          errorCode = ErrorCode.UnknownError;
      }

      throw new Error(errorCode);
    }
  }

  public async send<Q extends QueueSend>(
    queue: Q,
    data: QueuesSend[Q]
  ): Promise<true> {
    if (!this.#connection) throw new Error(ErrorCode.NotConnected);
    const dataEncode = this.#dataEncode<QueuesSend[Q]>(data);

    try {
      this.#connection.publish(queue, dataEncode);
      return true;
    } catch (error) {
      console.log(error);
      throw new Error(ErrorCode.MessageNotSent);
    }
  }

  public async request<Q extends QueueRequest>(
    queue: Q,
    data: QueuesRequest[Q]['message']
  ): Promise<QueuesRequest[Q]['response']> {
    if (!this.#connection) throw new Error(ErrorCode.NotConnected);
    const dataEncode = this.#dataEncode<QueuesRequest[Q]['message']>(data);

    try {
      const response = await this.#connection.request(queue, dataEncode);
      return this.#dataDecode<QueuesRequest[Q]['response']>(response.data);
    } catch (error) {
      if (error.code && error.code === '503') {
        throw new Error(ErrorCode.NoSubscriber);
      }
      throw new Error(ErrorCode.MessageNotSent);
    }
  }

  public subscribeSend<Q extends QueueSend>(
    queue: Q,
    callback: (message: QueuesSend[Q]) => void
  ): boolean {
    if (!this.#connection) throw new Error(ErrorCode.NotConnected);

    const subscription = this.#connection.subscribe(queue);
    if (!subscription) return false;

    (async () => {
      for await (const message of subscription) {
        callback(this.#dataDecode<QueuesSend[Q]>(message.data));
      }
    })();
    return true;
  }

  public subscribeRequest<Q extends QueueRequest>(
    queue: Q,
    callback: CallbackRequest
  ): true {
    if (!this.#connection) throw new Error(ErrorCode.NotConnected);
    const subscription = this.#connection.subscribe(queue);
    if (!subscription) throw new Error(ErrorCode.SubscribeNotSuccess);

    (async () => {
      for await (const message of subscription) {
        const callbackResult = await callback(this.#dataDecode(message.data));
        message.respond(this.#dataEncode(callbackResult));
      }
    })();

    return true;
  }

  get servers(): Servers {
    return this.#servers;
  }

  static getInstance(): MessageBroker {
    return instance;
  }
}
