export interface QueuesRequest {
  'request.test': {
    message: {
      text: string;
    };
    response: {
      data: string;
    };
  };
}

export type QueueRequest = keyof QueuesRequest;
