export interface QueuesSend {
  'send.test': {
    text: string;
  };
}

export type QueueSend = keyof QueuesSend;
